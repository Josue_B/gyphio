angular.module('app.services', [])

    .factory('SearchFactory', ['$http', '$q', function ($http, $q) {

        var search = {};
        var host = 'http://api.giphy.com/v1/gifs/random';
        var key = 'dc6zaTOxFJmzC';

        search.set_name = function (field) {
            search.name = field;
        };

        search.get_name = function () {
            return search.name;
        };

        search.get_gifs = function () {
            var deferred = $q.defer();
            var path = host + '?' + 'api_key=' + key;
            var full_path = path + '&tag=' + search.get_name();
            console.log(full_path);

            $http.get(full_path).then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                console.log('there was an error');
                deferred.reject(error);
            });
            return deferred.promise;

        };
        return search;

    }])

    .service('BlankService', [function () {
        
    }]);

