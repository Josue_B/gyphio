angular.module('app.controllers', [])

.controller('mainMenuCtrl', function($scope, $state, SearchFactory) {

    $scope.search = '';

    $scope.take_me = function(input) {
        //TODO check the input, if not input what do you want to display?
        SearchFactory.set_name(input);
        $state.go('giphySearch');
    }



})

.controller('giphySearchCtrl', function($scope, $q, $http, SearchFactory) {

    $scope.gifs = {};
    $scope.gifs.links = [];
    $scope.gifs.name = SearchFactory.get_name();


    var get_gif = function () {
        SearchFactory.get_gifs()
            .then(
                function(result) {
                    //TODO: only push to the array if the new giphy is not in the array.
                    $scope.gifs.links.push(result.data.image_url);
                    console.log($scope.gifs.links);
                },
                function (error) {
                    console.log('could not get the gifs', error);
                }
            );
    };


    var populate_with_gifs = function () {
        for (var i = 0; i < 10; i++) {
            get_gif();
        }
    };

    populate_with_gifs();

});
